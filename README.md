# springboot-cli

#### 介绍
自用springboot cli 基于springboot 2.2.1

#### 使用说明

1.  maven 3.3 或以上
2.  jdk 1.8 或以上

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
